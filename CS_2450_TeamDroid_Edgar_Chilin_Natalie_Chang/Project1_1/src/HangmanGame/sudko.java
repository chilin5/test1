/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/***************************************************************
* file: sudko.java
* author: Edgar Chilin, Natalie Chang
CS 2450- Programming Graphical User Interfaces
*
*
* assignment: program 1.1
* date last modified: 9/23/19
*
* purpose: This will be the form that will be 
*
****************************************************************/ 
package HangmanGame;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Thread.sleep;
import java.util.Calendar;
import java.util.GregorianCalendar;
//import javax.swing.JOptionPane;

/**
 *
 * @author Guest
 */
public class sudko extends javax.swing.JFrame {
///////////////
// global methods that will be used throughout the program 

    int Count = 0;
    int Correct = 0;
    int OverallScore;
    int Score = 540;
    String[] arrA = {"3", "5", "1", "9", "2"};
    String[] arrB = {"2", "9", "6", "8", "5", "7", "3", "1"};
    String[] arrC = {"4", "7", "2", "9", "3", "8"};
    String[] arrD = {"6", "1", "4", "2"};
    String[] arrE = {"1", "2", "3", "6", "8", "5", "4", "9"};
    String[] arrF = {"7", "5", "9", "6", "3"};
    String[] arrG = {"6", "7", "8", "1", "3", "4"};
    String[] arrI = {"9", "8", "3", "4", "5", "2", "7", "6"};
    String[] arrH = {"7", "4", "6", "8", "1"};

    /**
     * Creates new form sudko
     *
     * @param Score2
     */
    //method: Sudko
    //intializes the program each time 
    // at start up. Each time it opens it access an integer from a previous window form
    public sudko(int Score2) {
        initComponents();
        clock();
        ShowAll();
        // JOptionPane.showMessageDialog(null, Score2);
        OverallScore = Score2;
    }
//sudko
    // if the int contains no intiation or is not passed correctly it throughs this 
    //exception

    private sudko() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    //method: submitcheck
    //purpose: this will call the methods that will verify the text of each row
    // as well increment the count each time the user gets that value correct
    private void SubmitCheck() {
        checkBoxA();
        CheckB();
        CheckC();
        CheckD();
        CheckE();
        CheckF();
        CheckG();
        CheckH();
        CheckI();
        Count++;
        //JOptionPane.showMessageDialog(null, Correct);
        if (Correct >= 54) {
            HideAll();
            UpdateFile();
            dispose();
            EndScore es = new EndScore();
            es.setVisible(true);
            
        }
    }

    // method: updateFile
    // purpose: this will take the current score and the user's information
    // and save it to the specified file.
    private void UpdateFile() {
        int Sum = OverallScore + Score;
        String textToAppend = Integer.toString(Sum);
        try {

            FileWriter fileWriter = new FileWriter("/Users/Guest/Desktop/save.txt ", true); //Set true for append mode
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.println(textToAppend);  //New line
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // method: ShowAll
    //purpose:  This will reenable all the textfield in the program
    // setting them to visible 
    private void ShowAll() {
        A12.setVisible(true);
        A13.setVisible(true);
        A15.setVisible(true);
        A17.setVisible(true);
        A18.setVisible(true);

        B21.setVisible(true);
        B22.setVisible(true);
        B23.setVisible(true);
        B24.setVisible(true);
        B25.setVisible(true);
        B26.setVisible(true);
        B28.setVisible(true);
        B29.setVisible(true);

        C31.setVisible(true);
        C33.setVisible(true);
        C34.setVisible(true);
        C35.setVisible(true);
        C36.setVisible(true);
        C39.setVisible(true);

        D42.setVisible(true);
        D44.setVisible(true);
        D46.setVisible(true);
        D49.setVisible(true);

        E51.setVisible(true);
        E52.setVisible(true);
        E53.setVisible(true);
        E54.setVisible(true);
        E56.setVisible(true);
        E57.setVisible(true);
        E58.setVisible(true);
        E59.setVisible(true);

        F61.setVisible(true);
        F64.setVisible(true);
        F66.setVisible(true);
        F68.setVisible(true);

        G71.setVisible(true);
        G74.setVisible(true);
        G75.setVisible(true);
        G76.setVisible(true);
        G77.setVisible(true);
        G79.setVisible(true);

        H92.setVisible(true);
        H93.setVisible(true);
        H95.setVisible(true);
        H97.setVisible(true);
        H98.setVisible(true);

        I81.setVisible(true);
        I82.setVisible(true);
        I84.setVisible(true);
        I85.setVisible(true);
        I86.setVisible(true);
        I87.setVisible(true);
        I88.setVisible(true);
        I89.setVisible(true);

    }

    //method: hideall
    // purpose: this will conceall all the text field and show the user the
    // answer to the puzzle
    private void HideAll() {
        A12.setVisible(false);
        A13.setVisible(false);
        A15.setVisible(false);
        A17.setVisible(false);
        A18.setVisible(false);

        B21.setVisible(false);
        B22.setVisible(false);
        B23.setVisible(false);
        B24.setVisible(false);
        B25.setVisible(false);
        B26.setVisible(false);
        B28.setVisible(false);
        B29.setVisible(false);

        C31.setVisible(false);
        C33.setVisible(false);
        C34.setVisible(false);
        C35.setVisible(false);
        C36.setVisible(false);
        C39.setVisible(false);

        D42.setVisible(false);
        D44.setVisible(false);
        D46.setVisible(false);
        D49.setVisible(false);

        E51.setVisible(false);
        E52.setVisible(false);
        E53.setVisible(false);
        E54.setVisible(false);
        E56.setVisible(false);
        E57.setVisible(false);
        E58.setVisible(false);
        E59.setVisible(false);

        F61.setVisible(false);
        F64.setVisible(false);
        F66.setVisible(false);
        F68.setVisible(false);

        G71.setVisible(false);
        G74.setVisible(false);
        G75.setVisible(false);
        G76.setVisible(false);
        G77.setVisible(false);
        G79.setVisible(false);

        H92.setVisible(false);
        H93.setVisible(false);
        H95.setVisible(false);
        H97.setVisible(false);
        H98.setVisible(false);

        I81.setVisible(false);
        I82.setVisible(false);
        I84.setVisible(false);
        I85.setVisible(false);
        I86.setVisible(false);
        I87.setVisible(false);
        I88.setVisible(false);
        I89.setVisible(false);

    }

    //method: clock()
    //purpose: this will display the current date and time
    // in th gui which will be useing the gregorain calendar
    private void clock() {
        Thread th = new Thread() {
            public void run() {
                try {
                    for (;;) {
                        Calendar c1 = new GregorianCalendar();
                        int month = c1.get(Calendar.MONTH);
                        int day = c1.get(Calendar.DATE);
                        int year = c1.get(Calendar.YEAR);
                        int second = c1.get(Calendar.SECOND);
                        int min = c1.get(Calendar.MINUTE);
                        int hour = c1.get(Calendar.HOUR);
                        int am_pm = c1.get(Calendar.AM_PM);
                        String set = "";
                        if (am_pm == 1) {
                            set = "pm";
                        } else if (am_pm == 2) {
                            set = "am";
                        }
                        TimeLabel.setText(hour + ":" + min + ":" + second + " " + set + "\n"
                                + month + "/" + day + "/" + year);
                        sleep(1000);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
        th.start();
    }

    ////////---------The following A-I represents a different row of the grid 
    // Forgot to mention if the integer is correct then it will be change the textfield 
    // to empty and then hide it showing the user they have the correct answer
    // method: checkboxA
    // purpose: this will check to see if the integer entered exist in the specified row
    // but if it does not exist than it will change the text to red.
    private void checkBoxA() {
        if (arrA[0].equals(A12.getText().replace(" ", "").replace(" ", ""))) {
            A12.setVisible(false);
            A12.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            A12.setForeground(Color.red);
        } else {
            Score -= 10;
        }
        if (arrA[1].equals(A13.getText().replace(" ", "").replace(" ", ""))) {
            A13.setVisible(false);
            A13.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            A13.setForeground(Color.red);
        } else {

            A13.setForeground(Color.red);
        }
        if (arrA[2].equals(A15.getText().replace(" ", "").replace(" ", ""))) {
            A15.setVisible(false);
            A15.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            A15.setForeground(Color.red);
        } else {
            A15.setForeground(Color.red);
        }

        if (arrA[3].equals(A17.getText().replace(" ", "").replace(" ", ""))) {
            A17.setVisible(false);
            A17.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            A17.setForeground(Color.red);
        } else {
            A17.setForeground(Color.red);
        }

        if (arrA[4].equals(A18.getText().replace(" ", "").replace(" ", ""))) {
            A18.setVisible(false);
            A18.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            A18.setForeground(Color.red);
        } else {
            A18.setForeground(Color.red);
        }
        //   JOptionPane.showMessageDialog(null, Correct);
    }

    // method: checkB
    // purpose: this will check to see if the integer entered exist in the specified row
    // but if it does not exist than it will change the text to red
    private void CheckB() {

        if (arrB[0].equals(B21.getText().replace(" ", ""))) {
            B21.setVisible(false);
            B21.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            B21.setForeground(Color.red);
        } else {
            B21.setForeground(Color.red);
        }
        if (arrB[1].equals(B22.getText().replace(" ", ""))) {
            B22.setVisible(false);
            B22.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            B22.setForeground(Color.red);

        } else {
            B22.setForeground(Color.red);
        }
        if (arrB[2].equals(B23.getText().replace(" ", ""))) {
            B23.setVisible(false);
            B23.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            B23.setForeground(Color.red);

        } else {
            B23.setForeground(Color.red);

        }

        if (arrB[3].equals(B24.getText().replace(" ", ""))) {
            B24.setVisible(false);
            B24.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            B24.setForeground(Color.red);

        } else {
            B21.setForeground(Color.red);

        }

        if (arrB[4].equals(B25.getText().replace(" ", ""))) {
            B25.setVisible(false);
            B25.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            B25.setForeground(Color.red);

        } else {
            B25.setForeground(Color.red);

        }
        if (arrB[5].equals(B26.getText().replace(" ", ""))) {
            B26.setVisible(false);
            B26.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            B26.setForeground(Color.red);

        } else {
            B26.setForeground(Color.red);

        }
        if (arrB[7].equals(B29.getText().replace(" ", ""))) {
            B29.setVisible(false);
            B29.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            B29.setForeground(Color.red);

        } else {
            B29.setForeground(Color.red);

        }
        if (arrB[6].equals(B28.getText().replace(" ", ""))) {
            B28.setVisible(false);
            B28.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            B28.setForeground(Color.red);

        } else {
            B28.setForeground(Color.red);

        }
    }

    // method: checkc
    // purpose: this will check to see if the integer entered exist in the specified row
    // but if it does not exist than it will change the text to red
    private void CheckC() {
        if (arrC[0].equals(C31.getText().replace(" ", ""))) {
            C31.setVisible(false);
            C31.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            C31.setForeground(Color.red);

        } else {
            C31.setForeground(Color.red);

        }
        if (arrC[1].equals(C33.getText().replace(" ", ""))) {
            C33.setVisible(false);
            C33.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            C33.setForeground(Color.red);

        } else {
            C33.setForeground(Color.red);

        }
        if (arrC[2].equals(C34.getText().replace(" ", ""))) {
            C34.setVisible(false);
            C34.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            C34.setForeground(Color.red);

        } else {
            C34.setForeground(Color.red);

        }

        if (arrC[3].equals(C35.getText().replace(" ", ""))) {
            C35.setVisible(false);
            C35.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            C35.setForeground(Color.red);

        } else {
            C35.setForeground(Color.red);

        }

        if (arrC[4].equals(C36.getText().replace(" ", ""))) {
            C36.setVisible(false);
            C36.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            C36.setForeground(Color.red);

        } else {
            C36.setForeground(Color.red);

        }
        if (arrC[5].equals(C39.getText().replace(" ", ""))) {
            C39.setVisible(false);
            C39.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            C39.setForeground(Color.red);

        } else {
            Score -= 10;
            C39.setForeground(Color.red);

        }

    }

    // method: checkd
    // purpose: this will check to see if the integer entered exist in the specified row
    // but if it does not exist than it will change the text to red
    private void CheckD() {
        if (arrD[0].equals(D42.getText().replace(" ", ""))) {
            D42.setVisible(false);
            D42.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            D42.setForeground(Color.red);

        } else {
            D42.setForeground(Color.red);
        }
        if (arrD[1].equals(D44.getText().replace(" ", ""))) {
            D44.setVisible(false);
            D44.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            D44.setForeground(Color.red);
        } else {
            D44.setForeground(Color.red);
        }
        if (arrD[2].equals(D46.getText().replace(" ", ""))) {
            D46.setVisible(false);
            D46.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            D46.setForeground(Color.red);
        } else {
            D46.setForeground(Color.red);
        }

        if (arrD[3].equals(D49.getText().replace(" ", ""))) {
            D49.setVisible(false);
            D49.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            D46.setForeground(Color.red);
        } else {
            D46.setForeground(Color.red);
        }
    }

    // method: checkE
    // purpose: this will check to see if the integer entered exist in the specified row
    // but if it does not exist than it will change the text to red
    private void CheckE() {
        if (arrE[0].equals(E51.getText().replace(" ", ""))) {
            E51.setVisible(false);
            E51.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            E51.setForeground(Color.red);
        } else {
            E51.setForeground(Color.red);
        }
        if (arrE[1].equals(E52.getText().replace(" ", ""))) {
            E52.setVisible(false);
            E52.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            E52.setForeground(Color.red);
        } else {
            E52.setForeground(Color.red);
        }
        if (arrE[2].equals(E53.getText().replace(" ", ""))) {
            E53.setVisible(false);
            E53.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            E53.setForeground(Color.red);
        } else {
            E53.setForeground(Color.red);
        }

        if (arrE[3].equals(E54.getText().replace(" ", ""))) {
            E54.setVisible(false);
            E54.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            E54.setForeground(Color.red);
        } else {
            E54.setForeground(Color.red);
        }

        if (arrE[4].equals(E56.getText().replace(" ", ""))) {
            E56.setVisible(false);
            E56.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            E56.setForeground(Color.red);
        } else {
            E56.setForeground(Color.red);
        }
        if (arrE[5].equals(E57.getText().replace(" ", ""))) {
            E57.setVisible(false);
            E57.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            E57.setForeground(Color.red);
        } else {
            E57.setForeground(Color.red);
        }
        if (arrE[6].equals(E58.getText().replace(" ", ""))) {
            E58.setVisible(false);
            E58.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            E58.setForeground(Color.red);
        } else {
            E58.setForeground(Color.red);
        }
        if (arrE[7].equals(E59.getText().replace(" ", ""))) {
            E59.setVisible(false);
            E59.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            E59.setForeground(Color.red);
        } else {
            E59.setForeground(Color.red);
        }
    }

    // method: checkF
    // purpose: this will check to see if the integer entered exist in the specified row
    // but if it does not exist than it will change the text to red
    private void CheckF() {
        if (arrF[0].equals(F61.getText().replace(" ", ""))) {
            F61.setVisible(false);
            F61.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            F61.setForeground(Color.red);
        } else {
            F61.setForeground(Color.red);
        }
        if (arrF[1].equals(F64.getText().replace(" ", ""))) {
            F64.setVisible(false);
            F64.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            F64.setForeground(Color.red);
        } else {
            F64.setForeground(Color.red);
        }
        if (arrF[2].equals(F66.getText().replace(" ", ""))) {
            F66.setVisible(false);
            F66.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            F64.setForeground(Color.red);
        } else {
            F64.setForeground(Color.red);
        }

        if (arrF[3].equals(F68.getText().replace(" ", ""))) {
            F68.setVisible(false);
            F68.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            F68.setForeground(Color.red);
        } else {
            F68.setForeground(Color.red);
        }
    }

    // method: checkG
    // purpose: this will check to see if the integer entered exist in the specified row
    // but if it does not exist than it will change the text to red
    private void CheckG() {
        if (arrG[0].equals(G71.getText().replace(" ", ""))) {
            G71.setVisible(false);
            G71.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            G71.setForeground(Color.red);
        } else {
            G71.setForeground(Color.red);
        }
        if (arrG[1].equals(G74.getText().replace(" ", ""))) {
            G74.setVisible(false);
            G74.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            G74.setForeground(Color.red);
        } else {
            G74.setForeground(Color.red);

        }
        if (arrG[2].equals(G75.getText().replace(" ", ""))) {
            G75.setVisible(false);
            G75.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            G75.setForeground(Color.red);
        } else {
            G75.setForeground(Color.red);
        }

        if (arrG[3].equals(G76.getText().replace(" ", ""))) {
            G76.setVisible(false);
            G76.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            G76.setForeground(Color.red);
        } else {
            G76.setForeground(Color.red);
        }

        if (arrG[4].equals(G77.getText().replace(" ", ""))) {
            G77.setVisible(false);
            G77.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            G77.setForeground(Color.red);
        } else {
            G77.setForeground(Color.red);
        }
        if (arrG[5].equals(G79.getText().replace(" ", ""))) {
            G79.setVisible(false);
            G79.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            G79.setForeground(Color.red);
        } else {
            G79.setForeground(Color.red);
        }

    }

    // method: checkH
    // purpose: this will check to see if the integer entered exist in the specified row
    // but if it does not exist than it will change the text to red
    private void CheckH() {
        if (arrH[0].equals(H92.getText().replace(" ", ""))) {
            H92.setVisible(false);
            H92.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            H92.setForeground(Color.red);
        } else {
            H92.setForeground(Color.red);
        }
        if (arrH[1].equals(H93.getText().replace(" ", ""))) {
            H93.setVisible(false);
            H93.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            H93.setForeground(Color.red);
        } else {
            H93.setForeground(Color.red);
        }
        if (arrH[2].equals(H95.getText().replace(" ", ""))) {
            H95.setVisible(false);
            H95.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            H95.setForeground(Color.red);
        } else {
            H95.setForeground(Color.red);
        }

        if (arrH[3].equals(H97.getText().replace(" ", ""))) {
            H97.setVisible(false);
            H97.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            H97.setForeground(Color.red);
        } else {
            H97.setForeground(Color.red);
        }

        if (arrH[4].equals(H98.getText().replace(" ", ""))) {
            H98.setVisible(false);
            H98.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            H98.setForeground(Color.red);
        } else {
            H98.setForeground(Color.red);
        }

    }

    // method: checkI
    // purpose: this will check to see if the integer entered exist in the specified row
    // but if it does not exist than it will change the text to red
    private void CheckI() {
        if (arrI[0].equals(I81.getText().replace(" ", ""))) {
            I81.setVisible(false);
            I81.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            I81.setForeground(Color.red);
        } else {
            I81.setForeground(Color.red);
        }
        if (arrI[1].equals(I82.getText().replace(" ", ""))) {
            I82.setVisible(false);
            I82.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            I82.setForeground(Color.red);
        } else {
            I82.setForeground(Color.red);
        }
        if (arrI[2].equals(I84.getText().replace(" ", ""))) {
            I84.setVisible(false);
            I84.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            I84.setForeground(Color.red);
        } else {
            Score -= 10;
            I84.setForeground(Color.red);
        }

        if (arrI[3].equals(I85.getText().replace(" ", ""))) {
            I85.setVisible(false);
            I85.setText("");
            Correct += 1;
        } else if (Count == 0) {
            Score -= 10;
            I85.setForeground(Color.red);
        } else {
            I85.setForeground(Color.red);
        }

        if (arrI[4].equals(I86.getText().replace(" ", ""))) {
            I86.setVisible(false);
            I86.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            I86.setForeground(Color.red);
        } else {
            I86.setForeground(Color.red);
        }
        if (arrI[5].equals(I87.getText().replace(" ", ""))) {
            I87.setVisible(false);
            I87.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            I87.setForeground(Color.red);
        } else {
            I87.setForeground(Color.red);
        }
        if (arrI[6].equals(I88.getText().replace(" ", ""))) {
            I88.setVisible(false);
            I88.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            I87.setForeground(Color.red);
        } else {
            I87.setForeground(Color.red);
        }
        if (arrI[7].equals(I89.getText().replace(" ", ""))) {
            I89.setVisible(false);
            I89.setText("");
            Correct += 1;

        } else if (Count == 0) {
            Score -= 10;
            I89.setForeground(Color.red);
        } else {
            I89.setForeground(Color.red);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        A12 = new javax.swing.JTextField();
        A13 = new javax.swing.JTextField();
        A15 = new javax.swing.JTextField();
        A17 = new javax.swing.JTextField();
        A18 = new javax.swing.JTextField();
        B21 = new javax.swing.JTextField();
        B22 = new javax.swing.JTextField();
        B23 = new javax.swing.JTextField();
        B24 = new javax.swing.JTextField();
        B25 = new javax.swing.JTextField();
        B26 = new javax.swing.JTextField();
        B28 = new javax.swing.JTextField();
        B29 = new javax.swing.JTextField();
        C31 = new javax.swing.JTextField();
        C33 = new javax.swing.JTextField();
        C34 = new javax.swing.JTextField();
        C35 = new javax.swing.JTextField();
        C36 = new javax.swing.JTextField();
        C39 = new javax.swing.JTextField();
        D42 = new javax.swing.JTextField();
        D44 = new javax.swing.JTextField();
        D46 = new javax.swing.JTextField();
        D49 = new javax.swing.JTextField();
        E51 = new javax.swing.JTextField();
        E52 = new javax.swing.JTextField();
        E53 = new javax.swing.JTextField();
        E54 = new javax.swing.JTextField();
        E56 = new javax.swing.JTextField();
        E57 = new javax.swing.JTextField();
        E58 = new javax.swing.JTextField();
        E59 = new javax.swing.JTextField();
        F61 = new javax.swing.JTextField();
        F64 = new javax.swing.JTextField();
        F66 = new javax.swing.JTextField();
        F68 = new javax.swing.JTextField();
        G71 = new javax.swing.JTextField();
        G74 = new javax.swing.JTextField();
        G75 = new javax.swing.JTextField();
        G76 = new javax.swing.JTextField();
        G77 = new javax.swing.JTextField();
        G79 = new javax.swing.JTextField();
        I81 = new javax.swing.JTextField();
        I82 = new javax.swing.JTextField();
        I84 = new javax.swing.JTextField();
        I85 = new javax.swing.JTextField();
        I86 = new javax.swing.JTextField();
        I87 = new javax.swing.JTextField();
        I88 = new javax.swing.JTextField();
        I89 = new javax.swing.JTextField();
        H92 = new javax.swing.JTextField();
        H93 = new javax.swing.JTextField();
        H95 = new javax.swing.JTextField();
        H97 = new javax.swing.JTextField();
        H98 = new javax.swing.JTextField();
        background = new javax.swing.JLabel();
        Quit = new javax.swing.JButton();
        Submit = new javax.swing.JButton();
        TimeLabel = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                formKeyPressed(evt);
            }
        });

        jPanel1.setLayout(null);

        A12.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        A12.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        A12.setToolTipText("Enter a value from 1-9.\nIf value is greater than text turns red.\nIf the value is incorrect after submiting it turns red");
        A12.setBorder(null);
        A12.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                A12KeyPressed(evt);
            }
        });
        jPanel1.add(A12);
        A12.setBounds(56, 24, 30, 30);

        A13.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        A13.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        A13.setToolTipText("Enter a value from 1-9.\nIf value is greater than text turns red.\nIf the value is incorrect after submiting it turns red");
        A13.setBorder(null);
        A13.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                A13KeyPressed(evt);
            }
        });
        jPanel1.add(A13);
        A13.setBounds(90, 24, 30, 30);

        A15.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        A15.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        A15.setToolTipText("Enter a value from 1-9.\nIf value is greater than text turns red.\nIf the value is incorrect after submiting it turns red");
        A15.setBorder(null);
        A15.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                A15KeyPressed(evt);
            }
        });
        jPanel1.add(A15);
        A15.setBounds(160, 24, 30, 30);

        A17.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        A17.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        A17.setToolTipText("Enter a value from 1-9.\nIf value is greater than text turns red.\nIf the value is incorrect after submiting it turns red");
        A17.setBorder(null);
        A17.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                A17KeyPressed(evt);
            }
        });
        jPanel1.add(A17);
        A17.setBounds(232, 24, 30, 30);

        A18.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        A18.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        A18.setToolTipText("Enter a value from 1-9.\nIf value is greater than text turns red.\nIf the value is incorrect after submiting it turns red");
        A18.setBorder(null);
        A18.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                A18KeyPressed(evt);
            }
        });
        jPanel1.add(A18);
        A18.setBounds(270, 24, 30, 30);

        B21.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        B21.setToolTipText("Enter a value from 1-9.\nIf value is greater than text turns red.\nIf the value is incorrect after submiting it turns red");
        B21.setBorder(null);
        B21.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B21KeyPressed(evt);
            }
        });
        jPanel1.add(B21);
        B21.setBounds(22, 60, 30, 30);

        B22.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        B22.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        B22.setToolTipText("Enter a value from 1-9.\nIf value is greater than text turns red.\nIf the value is incorrect after submiting it turns red");
        B22.setBorder(null);
        B22.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B22KeyPressed(evt);
            }
        });
        jPanel1.add(B22);
        B22.setBounds(56, 60, 30, 30);

        B23.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        B23.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        B23.setToolTipText("Enter a value from 1-9.\nIf value is greater than text turns red.\nIf the value is incorrect after submiting it turns red");
        B23.setBorder(null);
        B23.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B23KeyPressed(evt);
            }
        });
        jPanel1.add(B23);
        B23.setBounds(90, 60, 30, 30);

        B24.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        B24.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        B24.setToolTipText("Enter a value from 1-9.\nIf value is greater than text turns red.\nIf the value is incorrect after submiting it turns red");
        B24.setBorder(null);
        B24.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B24KeyPressed(evt);
            }
        });
        jPanel1.add(B24);
        B24.setBounds(126, 60, 30, 30);

        B25.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        B25.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        B25.setToolTipText("Enter a value from 1-9.\nIf value is greater than text turns red.\nIf the value is incorrect after submiting it turns red");
        B25.setBorder(null);
        B25.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B25KeyPressed(evt);
            }
        });
        jPanel1.add(B25);
        B25.setBounds(160, 60, 30, 30);

        B26.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        B26.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        B26.setToolTipText("Enter a value from 1-9.\nIf value is greater than text turns red.\nIf the value is incorrect after submiting it turns red");
        B26.setBorder(null);
        B26.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B26KeyPressed(evt);
            }
        });
        jPanel1.add(B26);
        B26.setBounds(197, 60, 30, 30);

        B28.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        B28.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        B28.setToolTipText("Enter a value from 1-9.\nIf value is greater than text turns red.\nIf the value is incorrect after submiting it turns red");
        B28.setBorder(null);
        B28.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B28KeyPressed(evt);
            }
        });
        jPanel1.add(B28);
        B28.setBounds(270, 60, 30, 30);

        B29.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        B29.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        B29.setToolTipText("Enter a value from 1-9.\nIf value is greater than text turns red.\nIf the value is incorrect after submiting it turns red");
        B29.setBorder(null);
        B29.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                B29KeyPressed(evt);
            }
        });
        jPanel1.add(B29);
        B29.setBounds(303, 60, 30, 30);

        C31.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        C31.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        C31.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        C31.setBorder(null);
        C31.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                C31KeyPressed(evt);
            }
        });
        jPanel1.add(C31);
        C31.setBounds(20, 100, 30, 30);

        C33.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        C33.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        C33.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        C33.setBorder(null);
        C33.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                C33ActionPerformed(evt);
            }
        });
        C33.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                C33KeyPressed(evt);
            }
        });
        jPanel1.add(C33);
        C33.setBounds(90, 100, 30, 30);

        C34.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        C34.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        C34.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        C34.setBorder(null);
        C34.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                C34KeyPressed(evt);
            }
        });
        jPanel1.add(C34);
        C34.setBounds(126, 100, 30, 30);

        C35.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        C35.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        C35.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        C35.setBorder(null);
        C35.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                C35KeyPressed(evt);
            }
        });
        jPanel1.add(C35);
        C35.setBounds(160, 100, 30, 30);

        C36.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        C36.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        C36.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        C36.setBorder(null);
        C36.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                C36KeyPressed(evt);
            }
        });
        jPanel1.add(C36);
        C36.setBounds(197, 100, 30, 30);

        C39.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        C39.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        C39.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        C39.setBorder(null);
        C39.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                C39KeyPressed(evt);
            }
        });
        jPanel1.add(C39);
        C39.setBounds(303, 100, 30, 30);

        D42.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        D42.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        D42.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        D42.setBorder(null);
        D42.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                D42KeyPressed(evt);
            }
        });
        jPanel1.add(D42);
        D42.setBounds(56, 135, 30, 30);

        D44.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        D44.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        D44.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        D44.setBorder(null);
        D44.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                D44ActionPerformed(evt);
            }
        });
        D44.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                D44KeyPressed(evt);
            }
        });
        jPanel1.add(D44);
        D44.setBounds(126, 135, 30, 30);

        D46.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        D46.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        D46.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        D46.setBorder(null);
        D46.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                D46KeyPressed(evt);
            }
        });
        jPanel1.add(D46);
        D46.setBounds(197, 135, 30, 30);

        D49.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        D49.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        D49.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        D49.setBorder(null);
        D49.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                D49KeyPressed(evt);
            }
        });
        jPanel1.add(D49);
        D49.setBounds(303, 135, 30, 30);

        E51.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        E51.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        E51.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        E51.setBorder(null);
        E51.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                E51KeyPressed(evt);
            }
        });
        jPanel1.add(E51);
        E51.setBounds(20, 170, 30, 30);

        E52.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        E52.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        E52.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        E52.setBorder(null);
        E52.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                E52KeyPressed(evt);
            }
        });
        jPanel1.add(E52);
        E52.setBounds(56, 170, 30, 30);

        E53.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        E53.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        E53.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        E53.setBorder(null);
        E53.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                E53KeyPressed(evt);
            }
        });
        jPanel1.add(E53);
        E53.setBounds(90, 170, 30, 30);

        E54.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        E54.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        E54.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        E54.setBorder(null);
        E54.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                E54KeyPressed(evt);
            }
        });
        jPanel1.add(E54);
        E54.setBounds(126, 170, 30, 30);

        E56.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        E56.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        E56.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        E56.setBorder(null);
        E56.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                E56KeyPressed(evt);
            }
        });
        jPanel1.add(E56);
        E56.setBounds(197, 170, 30, 30);

        E57.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        E57.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        E57.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        E57.setBorder(null);
        E57.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                E57KeyPressed(evt);
            }
        });
        jPanel1.add(E57);
        E57.setBounds(232, 170, 30, 30);

        E58.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        E58.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        E58.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        E58.setBorder(null);
        E58.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                E58KeyPressed(evt);
            }
        });
        jPanel1.add(E58);
        E58.setBounds(270, 170, 30, 30);

        E59.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        E59.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        E59.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        E59.setBorder(null);
        E59.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                E59ActionPerformed(evt);
            }
        });
        E59.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                E59KeyPressed(evt);
            }
        });
        jPanel1.add(E59);
        E59.setBounds(303, 170, 30, 30);

        F61.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        F61.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        F61.setBorder(null);
        F61.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                F61KeyPressed(evt);
            }
        });
        jPanel1.add(F61);
        F61.setBounds(20, 210, 30, 30);

        F64.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        F64.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        F64.setBorder(null);
        F64.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                F64KeyPressed(evt);
            }
        });
        jPanel1.add(F64);
        F64.setBounds(126, 210, 30, 30);

        F66.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        F66.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        F66.setBorder(null);
        F66.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                F66KeyPressed(evt);
            }
        });
        jPanel1.add(F66);
        F66.setBounds(197, 210, 30, 30);

        F68.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        F68.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        F68.setBorder(null);
        F68.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                F68KeyPressed(evt);
            }
        });
        jPanel1.add(F68);
        F68.setBounds(270, 210, 30, 30);

        G71.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        G71.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        G71.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        G71.setBorder(null);
        G71.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                G71KeyPressed(evt);
            }
        });
        jPanel1.add(G71);
        G71.setBounds(20, 247, 30, 30);

        G74.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        G74.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        G74.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        G74.setBorder(null);
        G74.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                G74ActionPerformed(evt);
            }
        });
        G74.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                G74KeyPressed(evt);
            }
        });
        jPanel1.add(G74);
        G74.setBounds(126, 247, 30, 30);

        G75.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        G75.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        G75.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        G75.setBorder(null);
        G75.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                G75KeyPressed(evt);
            }
        });
        jPanel1.add(G75);
        G75.setBounds(160, 247, 30, 30);

        G76.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        G76.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        G76.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        G76.setBorder(null);
        G76.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                G76KeyPressed(evt);
            }
        });
        jPanel1.add(G76);
        G76.setBounds(197, 247, 30, 30);

        G77.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        G77.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        G77.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        G77.setBorder(null);
        G77.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                G77KeyPressed(evt);
            }
        });
        jPanel1.add(G77);
        G77.setBounds(232, 247, 30, 30);

        G79.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        G79.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        G79.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        G79.setBorder(null);
        G79.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                G79KeyPressed(evt);
            }
        });
        jPanel1.add(G79);
        G79.setBounds(303, 247, 30, 30);

        I81.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        I81.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        I81.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        I81.setBorder(null);
        I81.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                I81KeyPressed(evt);
            }
        });
        jPanel1.add(I81);
        I81.setBounds(20, 280, 30, 30);

        I82.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        I82.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        I82.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        I82.setBorder(null);
        I82.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                I82KeyPressed(evt);
            }
        });
        jPanel1.add(I82);
        I82.setBounds(56, 280, 30, 30);

        I84.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        I84.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        I84.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        I84.setBorder(null);
        I84.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                I84KeyPressed(evt);
            }
        });
        jPanel1.add(I84);
        I84.setBounds(126, 280, 30, 30);

        I85.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        I85.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        I85.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        I85.setBorder(null);
        I85.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                I85KeyPressed(evt);
            }
        });
        jPanel1.add(I85);
        I85.setBounds(160, 280, 30, 30);

        I86.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        I86.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        I86.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        I86.setBorder(null);
        I86.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                I86KeyPressed(evt);
            }
        });
        jPanel1.add(I86);
        I86.setBounds(197, 280, 30, 30);

        I87.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        I87.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        I87.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        I87.setBorder(null);
        I87.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                I87KeyPressed(evt);
            }
        });
        jPanel1.add(I87);
        I87.setBounds(232, 280, 30, 30);

        I88.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        I88.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        I88.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        I88.setBorder(null);
        I88.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                I88KeyPressed(evt);
            }
        });
        jPanel1.add(I88);
        I88.setBounds(270, 280, 30, 30);

        I89.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        I89.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        I89.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        I89.setBorder(null);
        I89.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                I89KeyPressed(evt);
            }
        });
        jPanel1.add(I89);
        I89.setBounds(303, 280, 30, 30);

        H92.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        H92.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        H92.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        H92.setBorder(null);
        H92.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                H92KeyPressed(evt);
            }
        });
        jPanel1.add(H92);
        H92.setBounds(56, 320, 30, 30);

        H93.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        H93.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        H93.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        H93.setBorder(null);
        H93.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                H93KeyPressed(evt);
            }
        });
        jPanel1.add(H93);
        H93.setBounds(90, 320, 30, 30);

        H95.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        H95.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        H95.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        H95.setBorder(null);
        H95.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                H95KeyPressed(evt);
            }
        });
        jPanel1.add(H95);
        H95.setBounds(160, 320, 30, 30);

        H97.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        H97.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        H97.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        H97.setBorder(null);
        H97.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                H97KeyPressed(evt);
            }
        });
        jPanel1.add(H97);
        H97.setBounds(232, 320, 30, 30);

        H98.setFont(new java.awt.Font("Lucida Grande", 0, 18)); // NOI18N
        H98.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        H98.setToolTipText("Enter a value from 1-9. If value is greater than text turns red. If the value is incorrect after submiting it turns red");
        H98.setBorder(null);
        H98.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                H98ActionPerformed(evt);
            }
        });
        H98.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                H98KeyPressed(evt);
            }
        });
        jPanel1.add(H98);
        H98.setBounds(270, 320, 30, 30);

        background.setBackground(new java.awt.Color(255, 255, 255));
        background.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Screen Shot 2019-10-05 at 6.07.28 PM.png"))); // NOI18N
        jPanel1.add(background);
        background.setBounds(20, -10, 380, 390);

        Quit.setText("Quit");
        Quit.setToolTipText("End the game and reveals highscore");
        Quit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                QuitActionPerformed(evt);
            }
        });

        Submit.setText("Submit");
        Submit.setToolTipText("checks to see if the solution is correct and will prompt you to the end screen if the data correct");
        Submit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SubmitActionPerformed(evt);
            }
        });

        TimeLabel.setText("jLabel1");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(Submit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Quit)
                .addContainerGap(15, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(TimeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(32, 32, 32))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(TimeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(Quit)
                    .addComponent(Submit)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 356, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /////////////////////These are methods that were accidently created during programing/////////////////////////////////////////////////////////  
    //overall will not do anything since tehy are not programmed
    private void C33ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_C33ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_C33ActionPerformed

    private void D44ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_D44ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_D44ActionPerformed

    private void E59ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_E59ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_E59ActionPerformed

    private void G74ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_G74ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_G74ActionPerformed

    private void H98ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_H98ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_H98ActionPerformed

    //////////////////////////////////////////////////////////////////////////////
    //method SubmitActionPerformed
    //purpose: calls the function that will verify the rows and column inputted in the
    // sudoku puzzle
    private void SubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SubmitActionPerformed
        SubmitCheck();
    }//GEN-LAST:event_SubmitActionPerformed

    ///////////////////////////////////////////////////////////
    // The following methods are all similar the only difference is that they target 
    // different textfields within the program
    // method: **KeyPressed
    // purpose: this will check to see if the values from 1-9 is being inputted
    // but if another value is entered than it wil disable the text box. however if you try to
    // enter more than 1 int it turns red. if you enter a new value it will turn back to black text

    private void A12KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_A12KeyPressed
        String text = A12.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                A12.setForeground(Color.BLACK);
                A12.setEditable(true);
            } else {
                A12.setEditable(false);
                A12.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                A12.setForeground(Color.BLACK);
                A12.setEditable(true);
            } else {
                A12.setForeground(Color.red);
                A12.setEditable(false);
            }
        }
    }//GEN-LAST:event_A12KeyPressed

    private void A15KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_A15KeyPressed
        String text = A15.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                A15.setForeground(Color.BLACK);
                A15.setEditable(true);
            } else {
                A15.setEditable(false);
                A15.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                A15.setForeground(Color.BLACK);
                A15.setEditable(true);
            } else {
                A15.setForeground(Color.red);
                A15.setEditable(false);
            }
        }
    }//GEN-LAST:event_A15KeyPressed

    private void A17KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_A17KeyPressed
        String text = A17.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                A17.setForeground(Color.BLACK);
                A17.setEditable(true);
            } else {
                A17.setEditable(false);
                A17.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                A17.setForeground(Color.BLACK);
                A17.setEditable(true);
            } else {
                A17.setForeground(Color.red);
                A17.setEditable(false);
            }
        }
    }//GEN-LAST:event_A17KeyPressed

    private void A18KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_A18KeyPressed
        String text = A18.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                A18.setForeground(Color.BLACK);
                A18.setEditable(true);
            } else {
                A18.setEditable(false);
                A18.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                A18.setForeground(Color.BLACK);
                A18.setEditable(true);
            } else {
                A18.setForeground(Color.red);
                A18.setEditable(false);
            }
        }
    }//GEN-LAST:event_A18KeyPressed

    private void B21KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B21KeyPressed
        String text = B21.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                B21.setForeground(Color.BLACK);
                B21.setEditable(true);
            } else {
                B21.setEditable(false);
                B21.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                B21.setForeground(Color.BLACK);
                B21.setEditable(true);
            } else {
                B21.setForeground(Color.red);
                B21.setEditable(false);
            }
        }
    }//GEN-LAST:event_B21KeyPressed

    private void B22KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B22KeyPressed
        String text = B22.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                B22.setForeground(Color.BLACK);
                B22.setEditable(true);
            } else {
                B22.setEditable(false);
                B22.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                B22.setForeground(Color.BLACK);
                B22.setEditable(true);
            } else {
                B22.setForeground(Color.red);
                B22.setEditable(false);
            }
        }
    }//GEN-LAST:event_B22KeyPressed

    private void B23KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B23KeyPressed
        String text = B23.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                B23.setForeground(Color.BLACK);
                B23.setEditable(true);
            } else {
                B23.setEditable(false);
                B23.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                B23.setForeground(Color.BLACK);
                B23.setEditable(true);
            } else {
                B23.setForeground(Color.red);
                B23.setEditable(false);
            }
        }
    }//GEN-LAST:event_B23KeyPressed

    private void B24KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B24KeyPressed
        String text = B24.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                B24.setForeground(Color.BLACK);
                B24.setEditable(true);
            } else {
                B24.setEditable(false);
                B24.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                B24.setForeground(Color.BLACK);
                B24.setEditable(true);
            } else {
                B24.setForeground(Color.red);
                B24.setEditable(false);
            }
        }
    }//GEN-LAST:event_B24KeyPressed

    private void B25KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B25KeyPressed
        String text = B25.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                B25.setForeground(Color.BLACK);
                B25.setEditable(true);
            } else {
                B25.setEditable(false);
                B25.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                B25.setForeground(Color.BLACK);
                B25.setEditable(true);
            } else {
                B25.setForeground(Color.red);
                B25.setEditable(false);
            }
        }
    }//GEN-LAST:event_B25KeyPressed

    private void B26KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B26KeyPressed
        String text = B26.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                B26.setForeground(Color.BLACK);
                B26.setEditable(true);
            } else {
                B26.setEditable(false);
                B26.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                B26.setForeground(Color.BLACK);
                B26.setEditable(true);
            } else {
                B26.setForeground(Color.red);
                B26.setEditable(false);
            }
        }
    }//GEN-LAST:event_B26KeyPressed

    private void B28KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B28KeyPressed
        String text = B28.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                B28.setForeground(Color.BLACK);
                B28.setEditable(true);
            } else {
                B28.setEditable(false);
                B28.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                B28.setForeground(Color.BLACK);
                B28.setEditable(true);
            } else {
                B28.setForeground(Color.red);
                B28.setEditable(false);
            }
        }
    }//GEN-LAST:event_B28KeyPressed

    private void B29KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_B29KeyPressed
        String text = B29.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                B29.setForeground(Color.BLACK);
                B29.setEditable(true);
            } else {
                B29.setEditable(false);
                B29.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                B29.setForeground(Color.BLACK);
                B29.setEditable(true);
            } else {
                B29.setForeground(Color.red);
                B29.setEditable(false);
            }
        }
    }//GEN-LAST:event_B29KeyPressed

    private void C31KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_C31KeyPressed
        String text = C31.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                C31.setForeground(Color.BLACK);
                C31.setEditable(true);
            } else {
                C31.setEditable(false);
                C31.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                C31.setForeground(Color.BLACK);
                C31.setEditable(true);
            } else {
                C31.setForeground(Color.red);
                C31.setEditable(false);
            }
        }
    }//GEN-LAST:event_C31KeyPressed

    private void C33KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_C33KeyPressed
        String text = C33.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                C33.setForeground(Color.BLACK);
                C33.setEditable(true);
            } else {
                C33.setEditable(false);
                C33.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                C33.setForeground(Color.BLACK);
                C33.setEditable(true);
            } else {
                C33.setForeground(Color.red);
                C33.setEditable(false);
            }
        }
    }//GEN-LAST:event_C33KeyPressed

    private void C34KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_C34KeyPressed
        String text = C34.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                C34.setForeground(Color.BLACK);
                C34.setEditable(true);
            } else {
                C34.setEditable(false);
                C34.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                C34.setForeground(Color.BLACK);
                C34.setEditable(true);
            } else {
                C34.setForeground(Color.red);
                C34.setEditable(false);
            }
        }
    }//GEN-LAST:event_C34KeyPressed

    private void C35KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_C35KeyPressed
        String text = C35.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                C35.setForeground(Color.BLACK);
                C35.setEditable(true);
            } else {
                C35.setEditable(false);
                C35.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                C35.setForeground(Color.BLACK);
                C35.setEditable(true);
            } else {
                C35.setForeground(Color.red);
                C35.setEditable(false);
            }
        }
    }//GEN-LAST:event_C35KeyPressed

    private void C36KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_C36KeyPressed
        String text = C36.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                C36.setForeground(Color.BLACK);
                C36.setEditable(true);
            } else {
                C36.setEditable(false);
                C36.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                C36.setForeground(Color.BLACK);
                C36.setEditable(true);
            } else {
                C36.setForeground(Color.red);
                C36.setEditable(false);
            }
        }
    }//GEN-LAST:event_C36KeyPressed

    private void C39KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_C39KeyPressed
        String text = C39.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                C39.setForeground(Color.BLACK);
                C39.setEditable(true);
            } else {
                C39.setEditable(false);
                C39.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                C39.setForeground(Color.BLACK);
                C39.setEditable(true);
            } else {
                C39.setForeground(Color.red);
                C39.setEditable(false);
            }
        }
    }//GEN-LAST:event_C39KeyPressed

    private void D42KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_D42KeyPressed
        String text = D42.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                D42.setForeground(Color.BLACK);
                D42.setEditable(true);
            } else {
                D42.setEditable(false);
                D42.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                D42.setForeground(Color.BLACK);
                D42.setEditable(true);
            } else {
                D42.setForeground(Color.red);
                D42.setEditable(false);
            }
        }
    }//GEN-LAST:event_D42KeyPressed

    private void D44KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_D44KeyPressed
        String text = D44.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                D44.setForeground(Color.BLACK);
                D44.setEditable(true);
            } else {
                D44.setEditable(false);
                D44.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                D44.setForeground(Color.BLACK);
                D44.setEditable(true);
            } else {
                D44.setForeground(Color.red);
                D44.setEditable(false);
            }
        }
    }//GEN-LAST:event_D44KeyPressed

    private void D46KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_D46KeyPressed
        String text = D46.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                D46.setForeground(Color.BLACK);
                D46.setEditable(true);
            } else {
                D46.setEditable(false);
                D46.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                D46.setForeground(Color.BLACK);
                D46.setEditable(true);
            } else {
                D46.setForeground(Color.red);
                D46.setEditable(false);
            }
        }
    }//GEN-LAST:event_D46KeyPressed

    private void D49KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_D49KeyPressed
        String text = D49.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                D49.setForeground(Color.BLACK);
                D49.setEditable(true);
            } else {
                D49.setEditable(false);
                D49.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                D49.setForeground(Color.BLACK);
                D49.setEditable(true);
            } else {
                D49.setForeground(Color.red);
                D49.setEditable(false);
            }
        }
    }//GEN-LAST:event_D49KeyPressed

    private void E51KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_E51KeyPressed
        String text = E51.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                E51.setForeground(Color.BLACK);
                E51.setEditable(true);
            } else {
                E51.setEditable(false);
                E51.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                E51.setForeground(Color.BLACK);
                E51.setEditable(true);
            } else {
                E51.setForeground(Color.red);
                E51.setEditable(false);
            }
        }
    }//GEN-LAST:event_E51KeyPressed

    private void E52KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_E52KeyPressed
        String text = E52.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                E52.setForeground(Color.BLACK);
                E52.setEditable(true);
            } else {
                E52.setEditable(false);
                E52.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                E52.setForeground(Color.BLACK);
                E52.setEditable(true);
            } else {
                E52.setForeground(Color.red);
                E52.setEditable(false);
            }
        }
    }//GEN-LAST:event_E52KeyPressed

    private void E53KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_E53KeyPressed
        String text = E53.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                E53.setForeground(Color.BLACK);
                E53.setEditable(true);
            } else {
                E53.setEditable(false);
                E53.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                E53.setForeground(Color.BLACK);
                E53.setEditable(true);
            } else {
                E53.setForeground(Color.red);
                E53.setEditable(false);
            }
        }
    }//GEN-LAST:event_E53KeyPressed

    private void E54KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_E54KeyPressed
        String text = E54.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                E54.setForeground(Color.BLACK);
                E54.setEditable(true);
            } else {
                E54.setEditable(false);
                E54.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                E54.setForeground(Color.BLACK);
                E54.setEditable(true);
            } else {
                E54.setForeground(Color.red);
                E54.setEditable(false);
            }
        }
    }//GEN-LAST:event_E54KeyPressed

    private void E56KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_E56KeyPressed
        String text = E56.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                E56.setForeground(Color.BLACK);
                E56.setEditable(true);
            } else {
                E56.setEditable(false);
                E56.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                E56.setForeground(Color.BLACK);
                E56.setEditable(true);
            } else {
                E56.setForeground(Color.red);
                E56.setEditable(false);
            }
        }
    }//GEN-LAST:event_E56KeyPressed

    private void E57KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_E57KeyPressed
        String text = E57.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                E57.setForeground(Color.BLACK);
                E57.setEditable(true);
            } else {
                E57.setEditable(false);
                E57.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                E57.setForeground(Color.BLACK);
                E57.setEditable(true);
            } else {
                E57.setForeground(Color.red);
                E57.setEditable(false);
            }
        }
    }//GEN-LAST:event_E57KeyPressed

    private void E58KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_E58KeyPressed
        String text = E58.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                E58.setForeground(Color.BLACK);
                E58.setEditable(true);
            } else {
                E58.setEditable(false);
                E58.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                E58.setForeground(Color.BLACK);
                E58.setEditable(true);
            } else {
                E58.setForeground(Color.red);
                E58.setEditable(false);
            }
        }
    }//GEN-LAST:event_E58KeyPressed

    private void E59KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_E59KeyPressed
        String text = E59.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                E59.setForeground(Color.BLACK);
                E59.setEditable(true);
            } else {
                E59.setEditable(false);
                E59.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                E59.setForeground(Color.BLACK);
                E59.setEditable(true);
            } else {
                E59.setForeground(Color.red);
                E59.setEditable(false);
            }
        }
    }//GEN-LAST:event_E59KeyPressed

    private void F61KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_F61KeyPressed
        String text = F61.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                F61.setForeground(Color.BLACK);
                F61.setEditable(true);
            } else {
                F61.setEditable(false);
                F61.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                F61.setForeground(Color.BLACK);
                F61.setEditable(true);
            } else {
                F61.setForeground(Color.red);
                F61.setEditable(false);
            }
        }
    }//GEN-LAST:event_F61KeyPressed

    private void F64KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_F64KeyPressed
        String text = F64.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                F64.setForeground(Color.BLACK);
                F64.setEditable(true);
            } else {
                F64.setEditable(false);
                F64.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                F64.setForeground(Color.BLACK);
                F64.setEditable(true);
            } else {
                F64.setForeground(Color.red);
                F64.setEditable(false);
            }
        }
    }//GEN-LAST:event_F64KeyPressed

    private void F66KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_F66KeyPressed
        String text = F66.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                F66.setForeground(Color.BLACK);
                F66.setEditable(true);
            } else {
                F66.setEditable(false);
                F66.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                F66.setForeground(Color.BLACK);
                F66.setEditable(true);
            } else {
                F66.setForeground(Color.red);
                F66.setEditable(false);
            }
        }
    }//GEN-LAST:event_F66KeyPressed

    private void F68KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_F68KeyPressed
        String text = F68.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                F68.setForeground(Color.BLACK);
                F68.setEditable(true);
            } else {
                F68.setEditable(false);
                F68.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                F68.setForeground(Color.BLACK);
                F68.setEditable(true);
            } else {
                F68.setForeground(Color.red);
                F68.setEditable(false);
            }
        }
    }//GEN-LAST:event_F68KeyPressed

    private void G71KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_G71KeyPressed
        String text = G71.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                G71.setForeground(Color.BLACK);
                G71.setEditable(true);
            } else {
                G71.setEditable(false);
                G71.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                G71.setForeground(Color.BLACK);
                G71.setEditable(true);
            } else {
                G71.setForeground(Color.red);
                G71.setEditable(false);
            }
        }
    }//GEN-LAST:event_G71KeyPressed

    private void G74KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_G74KeyPressed
        String text = G74.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                G74.setForeground(Color.BLACK);
                G74.setEditable(true);
            } else {
                G74.setEditable(false);
                G74.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                G74.setForeground(Color.BLACK);
                G74.setEditable(true);
            } else {
                G74.setForeground(Color.red);
                G74.setEditable(false);
            }
        }
    }//GEN-LAST:event_G74KeyPressed

    private void G75KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_G75KeyPressed
        String text = G75.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                G75.setForeground(Color.BLACK);
                G75.setEditable(true);
            } else {
                G75.setEditable(false);
                G75.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                G75.setForeground(Color.BLACK);
                G75.setEditable(true);
            } else {
                G75.setForeground(Color.red);
                G75.setEditable(false);
            }
        }
    }//GEN-LAST:event_G75KeyPressed

    private void G76KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_G76KeyPressed
        String text = G76.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                G76.setForeground(Color.BLACK);
                G76.setEditable(true);
            } else {
                G76.setEditable(false);
                G76.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                G76.setForeground(Color.BLACK);
                G76.setEditable(true);
            } else {
                G76.setForeground(Color.red);
                G76.setEditable(false);
            }
        }
    }//GEN-LAST:event_G76KeyPressed

    private void G77KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_G77KeyPressed
        String text = G77.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                G77.setForeground(Color.BLACK);
                G77.setEditable(true);
            } else {
                G77.setEditable(false);
                G77.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                G77.setForeground(Color.BLACK);
                G77.setEditable(true);
            } else {
                G77.setForeground(Color.red);
                G77.setEditable(false);
            }
        }
    }//GEN-LAST:event_G77KeyPressed

    private void G79KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_G79KeyPressed
        String text = G79.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                G79.setForeground(Color.BLACK);
                G79.setEditable(true);
            } else {
                G79.setEditable(false);
                G79.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                G79.setForeground(Color.BLACK);
                G79.setEditable(true);
            } else {
                G79.setForeground(Color.red);
                G79.setEditable(false);
            }
        }
    }//GEN-LAST:event_G79KeyPressed

    private void I81KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_I81KeyPressed
        String text = I81.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                I81.setForeground(Color.BLACK);
                I81.setEditable(true);
            } else {
                I81.setEditable(false);
                I81.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                I81.setForeground(Color.BLACK);
                I81.setEditable(true);
            } else {
                I81.setForeground(Color.red);
                I81.setEditable(false);
            }
        }
    }//GEN-LAST:event_I81KeyPressed

    private void I82KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_I82KeyPressed
        String text = I82.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                I82.setForeground(Color.BLACK);
                I82.setEditable(true);
            } else {
                I82.setEditable(false);
                I82.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                I82.setForeground(Color.BLACK);
                I82.setEditable(true);
            } else {
                I82.setForeground(Color.red);
                I82.setEditable(false);
            }
        }
    }//GEN-LAST:event_I82KeyPressed

    private void I84KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_I84KeyPressed
        String text = I84.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                I84.setForeground(Color.BLACK);
                I84.setEditable(true);
            } else {
                I84.setEditable(false);
                I84.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                I84.setForeground(Color.BLACK);
                I84.setEditable(true);
            } else {
                I84.setForeground(Color.red);
                I84.setEditable(false);
            }
        }
    }//GEN-LAST:event_I84KeyPressed

    private void I85KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_I85KeyPressed
        String text = I85.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                I85.setForeground(Color.BLACK);
                I85.setEditable(true);
            } else {
                I85.setEditable(false);
                I85.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                I85.setForeground(Color.BLACK);
                I85.setEditable(true);
            } else {
                I85.setForeground(Color.red);
                I85.setEditable(false);
            }
        }
    }//GEN-LAST:event_I85KeyPressed

    private void formKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyPressed
        if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        }
    }//GEN-LAST:event_formKeyPressed

    private void A13KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_A13KeyPressed
        String text = A13.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                A13.setForeground(Color.BLACK);
                A13.setEditable(true);
            } else {
                A13.setEditable(false);
                A13.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                A13.setForeground(Color.BLACK);
                A13.setEditable(true);
            } else {
                A13.setForeground(Color.red);
                A13.setEditable(false);
            }
        }
    }//GEN-LAST:event_A13KeyPressed

    private void I86KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_I86KeyPressed
        String text = I86.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                I86.setForeground(Color.BLACK);
                I86.setEditable(true);
            } else {
                I86.setEditable(false);
                I86.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                I86.setForeground(Color.BLACK);
                I86.setEditable(true);
            } else {
                I86.setForeground(Color.red);
                I86.setEditable(false);
            }
        }
    }//GEN-LAST:event_I86KeyPressed

    private void I87KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_I87KeyPressed
        String text = I87.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                I87.setForeground(Color.BLACK);
                I87.setEditable(true);
            } else {
                I87.setEditable(false);
                I87.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                I87.setForeground(Color.BLACK);
                I87.setEditable(true);
            } else {
                I87.setForeground(Color.red);
                I87.setEditable(false);
            }
        }
    }//GEN-LAST:event_I87KeyPressed

    private void I88KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_I88KeyPressed
        String text = I88.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                I88.setForeground(Color.BLACK);
                I88.setEditable(true);
            } else {
                I88.setEditable(false);
                I88.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                I88.setForeground(Color.BLACK);
                I88.setEditable(true);
            } else {
                I88.setForeground(Color.red);
                I88.setEditable(false);
            }
        }
    }//GEN-LAST:event_I88KeyPressed

    private void I89KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_I89KeyPressed
        String text = I89.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                I89.setForeground(Color.BLACK);
                I89.setEditable(true);
            } else {
                I89.setEditable(false);
                I89.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                I89.setForeground(Color.BLACK);
                I89.setEditable(true);
            } else {
                I89.setForeground(Color.red);
                I89.setEditable(false);
            }
        }
    }//GEN-LAST:event_I89KeyPressed

    private void H92KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_H92KeyPressed
        String text = H92.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                H92.setForeground(Color.BLACK);
                H92.setEditable(true);
            } else {
                H92.setEditable(false);
                H92.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                H92.setForeground(Color.BLACK);
                H92.setEditable(true);
            } else {
                H92.setForeground(Color.red);
                H92.setEditable(false);
            }
        }
    }//GEN-LAST:event_H92KeyPressed

    private void H93KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_H93KeyPressed
        String text = H93.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                H93.setForeground(Color.BLACK);
                H93.setEditable(true);
            } else {
                H93.setEditable(false);
                H93.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                H93.setForeground(Color.BLACK);
                H93.setEditable(true);
            } else {
                H93.setForeground(Color.red);
                H93.setEditable(false);
            }
        }
    }//GEN-LAST:event_H93KeyPressed

    private void H95KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_H95KeyPressed
        String text = H95.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                H95.setForeground(Color.BLACK);
                H95.setEditable(true);
            } else {
                H95.setEditable(false);
                H95.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                H95.setForeground(Color.BLACK);
                H95.setEditable(true);
            } else {
                H95.setForeground(Color.red);
                H95.setEditable(false);
            }
        }
    }//GEN-LAST:event_H95KeyPressed

    private void H97KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_H97KeyPressed
        String text = H97.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                H97.setForeground(Color.BLACK);
                H97.setEditable(true);
            } else {
                H97.setEditable(false);
                H97.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                H97.setForeground(Color.BLACK);
                H97.setEditable(true);
            } else {
                H97.setForeground(Color.red);
                H97.setEditable(false);
            }
        }
    }//GEN-LAST:event_H97KeyPressed

    private void H98KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_H98KeyPressed
        String text = H98.getText();
        int length = text.length();
        // JOptionPane.showMessageDialog(null, length);
        if (evt.getKeyChar() >= '1' && evt.getKeyChar() <= '9') {
            if (length < 1) {
                H98.setForeground(Color.BLACK);
                H98.setEditable(true);
            } else {
                H98.setEditable(false);
                H98.setForeground(Color.RED);
            }
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_F2) {
            HangmanCredit hc = new HangmanCredit();
            hc.setVisible(true);
        } else if (evt.getExtendedKeyCode() == KeyEvent.VK_ESCAPE) {
            dispose();
        } else {
            if (evt.getExtendedKeyCode() == KeyEvent.VK_BACK_SPACE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_DELETE
                    || evt.getExtendedKeyCode() == KeyEvent.VK_RIGHT
                    || evt.getExtendedKeyCode() == KeyEvent.VK_LEFT) {
                H98.setForeground(Color.BLACK);
                H98.setEditable(true);
            } else {
                H98.setForeground(Color.red);
                H98.setEditable(false);
            }
        }
    }//GEN-LAST:event_H98KeyPressed

    //method : QuitActionPerformed
    //purpose : this will reveal the user the answer and ask them to save the program
    // as well reveal them the answer of the program then show them the end screen
    private void QuitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_QuitActionPerformed
        //   HideAll();
        //   UpdateFile();
        dispose();
        EndScore es = new EndScore();
        es.setVisible(true);
    }//GEN-LAST:event_QuitActionPerformed

    //purpose: callAction
    //method: this will dipose the current screen then show the end score screen
    private void callAction() {
        HideAll();
        UpdateFile();
        dispose();
        EndScore es = new EndScore();
        es.setVisible(true);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(sudko.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(sudko.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(sudko.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(sudko.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new sudko().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField A12;
    private javax.swing.JTextField A13;
    private javax.swing.JTextField A15;
    private javax.swing.JTextField A17;
    private javax.swing.JTextField A18;
    private javax.swing.JTextField B21;
    private javax.swing.JTextField B22;
    private javax.swing.JTextField B23;
    private javax.swing.JTextField B24;
    private javax.swing.JTextField B25;
    private javax.swing.JTextField B26;
    private javax.swing.JTextField B28;
    private javax.swing.JTextField B29;
    private javax.swing.JTextField C31;
    private javax.swing.JTextField C33;
    private javax.swing.JTextField C34;
    private javax.swing.JTextField C35;
    private javax.swing.JTextField C36;
    private javax.swing.JTextField C39;
    private javax.swing.JTextField D42;
    private javax.swing.JTextField D44;
    private javax.swing.JTextField D46;
    private javax.swing.JTextField D49;
    private javax.swing.JTextField E51;
    private javax.swing.JTextField E52;
    private javax.swing.JTextField E53;
    private javax.swing.JTextField E54;
    private javax.swing.JTextField E56;
    private javax.swing.JTextField E57;
    private javax.swing.JTextField E58;
    private javax.swing.JTextField E59;
    private javax.swing.JTextField F61;
    private javax.swing.JTextField F64;
    private javax.swing.JTextField F66;
    private javax.swing.JTextField F68;
    private javax.swing.JTextField G71;
    private javax.swing.JTextField G74;
    private javax.swing.JTextField G75;
    private javax.swing.JTextField G76;
    private javax.swing.JTextField G77;
    private javax.swing.JTextField G79;
    private javax.swing.JTextField H92;
    private javax.swing.JTextField H93;
    private javax.swing.JTextField H95;
    private javax.swing.JTextField H97;
    private javax.swing.JTextField H98;
    private javax.swing.JTextField I81;
    private javax.swing.JTextField I82;
    private javax.swing.JTextField I84;
    private javax.swing.JTextField I85;
    private javax.swing.JTextField I86;
    private javax.swing.JTextField I87;
    private javax.swing.JTextField I88;
    private javax.swing.JTextField I89;
    private javax.swing.JButton Quit;
    private javax.swing.JButton Submit;
    private javax.swing.JLabel TimeLabel;
    private javax.swing.JLabel background;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
