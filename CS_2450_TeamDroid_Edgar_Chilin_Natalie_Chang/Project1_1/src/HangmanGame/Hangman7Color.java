package HangmanGame;
/***************************************************************
* file: Hangman7Color.java
* author: Edgar Chilin, Natalie Chang
CS 2450- Programming Graphical User Interfaces
*
*
* assignment: program 1.1
* date last modified: 9/23/19
*
* purpose: This will be the form that controls the buttons for colors as well
* give a label a certain color text and change it's overall color.
*
****************************************************************/ 
import java.awt.*;
//import java.swing.*;
import java.awt.Color;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import static java.lang.Thread.sleep;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;
import javax.swing.JOptionPane;
import javax.swing.UIManager;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Guest
 */
public class Hangman7Color extends javax.swing.JFrame {
    // Global Variables
    // purpose: global variables at there default stage which
    // will be called through the program 
    
   int OverallScore = 0;
    String textColor = "";
    String textBC = "";
    int count2 = 0;
    int Button1 = 0;
    int Score2 = 0;
    Color set;

    /**
     * Creates new form Hangman7Color
     * calls specific methods required to be started at start up
     * @param Score1
     */
    public Hangman7Color(int Score1) {
        initComponents();
        OverallScore = Score1;
        textColor = randomColor();
        textBC = ChangeColor();
        ColorChoice.setText(textColor);
        setColor();
        ClearDisplay();
        ColorButton1();
        clock();
    }

    private Hangman7Color() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    // method: SaveFile
    //purpose: Saves the score into a file 
    public void SaveFile(){
        try{
        FileWriter foo = new FileWriter("save.txt");
        foo.close(); 
        }catch(IOException e){
            e.fillInStackTrace();
        }
        
    }
    // method UpdateFile 
    //purpose: this will add the current score to the main file 
     private void UpdateFile() {
        try {
            
            String textToAppend =  "" + Score2;
            FileWriter fileWriter = new FileWriter("save.txt", true); //Set true for append mode
            PrintWriter printWriter = new PrintWriter(fileWriter);
            printWriter.println(textToAppend);  //New line
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    
    
    //method: clock()
    //purpose: this will display the current date and time
    // in th gui which will be useing the gregorain calendar
    private void clock() {
        Thread th = new Thread() {
            public void run() {
                try {
                    for (;;) {
                        Calendar c1 = new GregorianCalendar();
                        int month = c1.get(Calendar.MONTH);
                        int day = c1.get(Calendar.DATE);
                        int year = c1.get(Calendar.YEAR);
                        int second = c1.get(Calendar.SECOND);
                        int min = c1.get(Calendar.MINUTE);
                        int hour = c1.get(Calendar.HOUR);
                        int am_pm = c1.get(Calendar.AM_PM);
                        String set = "";
                        if (am_pm == 1) {
                            set = "pm";
                        } else if (am_pm == 2) {
                            set = "am";
                        }
                        TimeLabel.setText(hour + ":" + min + ":" + second + " " + set + "\n"
                                + month + "/" + day + "/" + year);
                        sleep(1000);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        };
        th.start();
    }

    // method: randomcolor
    // purpose: choose a random color that will be given to one of the global
    // varaibles of the program which will be displayed in the screen 
    private String randomColor() {
        String[] color = new String[5];
        color[0] = "red";
        color[1] = "yellow";
        color[2] = "green";
        color[3] = "purple";
        color[4] = "blue";
        //  color[4] = "climbing";
        Random random = new Random();
        int number = random.nextInt(5);
        return color[number];
    }

    // method: setColor
    // purpose: will set the color of the word to some random colors
    private void setColor() {
        if (null != textBC) {
            switch (textBC) {
                case "red":
                    set = Color.red;
                    ColorChoice.setForeground(set);
                    break;
                case "yellow":
                    set = Color.yellow;
                    ColorChoice.setForeground(set);
                    break;
                case "purple":
                    set = Color.MAGENTA;
                    ColorChoice.setForeground(set);
                    break;
                case "green":
                    set = Color.green;
                    ColorChoice.setForeground(set);
                    break;
                case "blue":
                    set = Color.blue;
                    ColorChoice.setForeground(set);
                    break;
                default:
                    break;
            }
        }
    }
    // method ChangeColor
    // purpose: will change the color of text color something random
    // avoid from the same word from being used
    private String ChangeColor() {
        String[] color = new String[4];
        if (null != textColor) {
            switch (textColor) {
                case "red":
                    color[0] = "green";
                    color[1] = "yellow";
                    color[2] = "purple";
                    color[3] = "blue";
                    break;
                case "yellow":
                    color[0] = "green";
                    color[1] = "red";
                    color[2] = "purple";
                    color[3] = "blue";
                    break;
                case "green":
                    color[0] = "red";
                    color[1] = "yellow";
                    color[2] = "purple";
                    color[3] = "blue";
                    break;
                case "purple":
                    color[0] = "green";
                    color[1] = "yellow";
                    color[2] = "red";
                    color[3] = "blue";
                    break;
                 case "blue":
                    color[0] = "green";
                    color[1] = "yellow";
                    color[2] = "purple";
                    color[3] = "red";
                    break;
                default:
                    break;
            }
        }
        Random random = new Random();
        int number = random.nextInt(3);
        return color[number];
    }
    
    // method callfeature
    // purpose this is requird to call each time they correctly
    // chose the right button for the program 
    private void CallFeatures() {
        textColor = randomColor();
        textBC = ChangeColor();
        ColorChoice.setText(textColor);
        setColor();
        ClearDisplay();
        ColorButton1();
       // count2++;
    }
    // method verifycolor
    // purpose will check how many times you have ran through the program 
    // as well verify if th button you selected has the right color
    // then it will increment the score
    private void VerifyColor(String choice) {
        if ("red".equals(choice) && count2 <= 5
                && textColor.equals(choice)) {
            CallFeatures();
            Score2 += 100;

        } else if ("yellow".equals(choice) && count2 <= 5
                && textColor.equals(choice)) {
            CallFeatures();
            Score2 += 100;

        } else if ("purple".equals(choice) && count2 <= 5
                && textColor.equals(choice)) {
            CallFeatures();
            Score2 += 10;

        } else if ("green".equals(choice) && count2 <= 5
                && textColor.equals(choice)) {
            CallFeatures();
            Score2 += 100;
        }else if ("blue".equals(choice) && count2 <= 5
                && textColor.equals(choice)) {
            CallFeatures();
            Score2 += 100;
        }
        
        if(count2 >= 5){
            End();
        }
        count2++;

    }
    // method: end
    // purpose: End the program by save the file and
    // then it will dispose the current screen. finally call the end screen
    private void End(){
       // UpdateFile();
        dispose();
        int Sum  = OverallScore +Score2;
       // Hangman4GameOver h4 = new Hangman4GameOver();
       // h4.setVisible(true);
      new sudko(Sum).setVisible(true);
     
    }
    // mehtod: clear display
    // purpose: hides all content and it disables it as well 
    private void ClearDisplay() {
        red.setVisible(false);
        green.setVisible(false);
        yellow.setVisible(false);
        purple.setVisible(false);
        //______________________________________
        red.setEnabled(false);
        green.setEnabled(false);
        yellow.setEnabled(false);
        purple.setEnabled(false);

        ////////////////////////////////////////////////////////
        red4.setVisible(false);
        green4.setVisible(false);
        yellow4.setVisible(false);
        purple4.setVisible(false);
        //______________________________________
        red4.setEnabled(false);
        green4.setEnabled(false);
        yellow4.setEnabled(false);
        purple4.setEnabled(false);

        ///////////////////////////////////////////////////////
        red5.setVisible(false);
        green5.setVisible(false);
        yellow5.setVisible(false);
        purple5.setVisible(false);
        //______________________________________
        red5.setEnabled(false);
        green5.setEnabled(false);
        yellow5.setEnabled(false);
        purple5.setEnabled(false);

        //////////////////////////////////////////////////////
        red6.setVisible(false);
        green6.setVisible(false);
        yellow6.setVisible(false);
        purple6.setVisible(false);
        //______________________________________
        red6.setEnabled(false);
        green6.setEnabled(false);
        yellow6.setEnabled(false);
        purple6.setEnabled(false);
        
        
        //////////////////////////////////////////////////////
         blue1.setVisible(false);
        blue2.setVisible(false);
        blue3.setVisible(false);
        blue4.setVisible(false);
        //______________________________________
        blue1.setEnabled(false);
        blue2.setEnabled(false);
        blue3.setEnabled(false);
        blue4.setEnabled(false);
    }

    
    // mehtod Colorbutton1
    // purpose will randomly choose which buttons
    // will be visible and can be seen by the user
    private void ColorButton1() {
        ClearDisplay();
        Random random = new Random();
        Button1 = random.nextInt(4);
        switch (Button1) {
            case 0:
                red.setVisible(true);
                green4.setVisible(true);
                yellow5.setVisible(true);
                purple6.setVisible(true);
                blue1.setVisible(true);
                //______________________________________
                red.setEnabled(true);
                green4.setEnabled(true);
                yellow5.setEnabled(true);
                purple6.setEnabled(true);
                blue1.setEnabled(true);
                break;
            case 1:
                red4.setVisible(true);
                green5.setVisible(true);
                yellow6.setVisible(true);
                purple.setVisible(true);
                blue2.setVisible(true);
                //______________________________________
                red4.setEnabled(true);
                green5.setEnabled(true);
                yellow6.setEnabled(true);
                purple.setEnabled(true);
                blue2.setEnabled(true);

                break;
            case 2:
                red5.setVisible(true);
                green6.setVisible(true);
                yellow.setVisible(true);
                purple4.setVisible(true);
                blue3.setVisible(true);
                //______________________________________
                red5.setEnabled(true);
                green6.setEnabled(true);
                yellow.setEnabled(true);
                purple4.setEnabled(true);
                blue3.setEnabled(true);
   
                break;
            case 3:
                red6.setVisible(true);
                green.setVisible(true);
                yellow4.setVisible(true);
                purple5.setVisible(true);
                blue4.setVisible(true);
                //______________________________________
                red6.setEnabled(true);
                green.setEnabled(true);
                yellow4.setEnabled(true);
                purple5.setEnabled(true);
                blue4.setEnabled(true);
                
                break;
            default:
                break;
        }

    }
    
    // method: different colorButtons
    // purpose: first assigns it a color then it will
    //verify if was the right color that was chosen

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TimeLabel = new javax.swing.JLabel();
        ColorChoice = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        green = new javax.swing.JButton();
        purple = new javax.swing.JButton();
        red = new javax.swing.JButton();
        yellow = new javax.swing.JButton();
        red4 = new javax.swing.JButton();
        green4 = new javax.swing.JButton();
        purple4 = new javax.swing.JButton();
        yellow4 = new javax.swing.JButton();
        purple5 = new javax.swing.JButton();
        green5 = new javax.swing.JButton();
        red5 = new javax.swing.JButton();
        yellow5 = new javax.swing.JButton();
        yellow6 = new javax.swing.JButton();
        purple6 = new javax.swing.JButton();
        green6 = new javax.swing.JButton();
        red6 = new javax.swing.JButton();
        blue1 = new javax.swing.JButton();
        blue2 = new javax.swing.JButton();
        blue3 = new javax.swing.JButton();
        blue4 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        TimeLabel.setText("jLabel3");

        ColorChoice.setFont(new java.awt.Font("Lucida Grande", 0, 48)); // NOI18N
        ColorChoice.setText("jLabel1");

        jPanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jPanel1MouseExited(evt);
            }
        });
        jPanel1.setLayout(null);

        green.setIcon(new javax.swing.ImageIcon(getClass().getResource("/green.png"))); // NOI18N
        green.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box\n");
        green.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                greenMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                greenMouseExited(evt);
            }
        });
        green.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                greenActionPerformed(evt);
            }
        });
        jPanel1.add(green);
        green.setBounds(0, 0, 130, 120);

        purple.setIcon(new javax.swing.ImageIcon(getClass().getResource("/purple.png"))); // NOI18N
        purple.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        purple.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                purpleMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                purpleMouseExited(evt);
            }
        });
        purple.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                purpleActionPerformed(evt);
            }
        });
        jPanel1.add(purple);
        purple.setBounds(60, 10, 130, 100);

        red.setIcon(new javax.swing.ImageIcon(getClass().getResource("/red.png"))); // NOI18N
        red.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        red.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                redMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                redMouseExited(evt);
            }
        });
        red.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                redActionPerformed(evt);
            }
        });
        jPanel1.add(red);
        red.setBounds(80, 30, 130, 130);

        yellow.setIcon(new javax.swing.ImageIcon(getClass().getResource("/yellow.png"))); // NOI18N
        yellow.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        yellow.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                yellowMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                yellowMouseExited(evt);
            }
        });
        yellow.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                yellowActionPerformed(evt);
            }
        });
        jPanel1.add(yellow);
        yellow.setBounds(60, 30, 120, 120);

        red4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/red.png"))); // NOI18N
        red4.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        red4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                red4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                red4MouseExited(evt);
            }
        });
        red4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                red4ActionPerformed(evt);
            }
        });
        jPanel1.add(red4);
        red4.setBounds(300, 40, 130, 120);

        green4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/green.png"))); // NOI18N
        green4.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        green4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                green4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                green4MouseExited(evt);
            }
        });
        green4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                green4ActionPerformed(evt);
            }
        });
        jPanel1.add(green4);
        green4.setBounds(300, 20, 130, 120);

        purple4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/purple.png"))); // NOI18N
        purple4.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        purple4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                purple4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                purple4MouseExited(evt);
            }
        });
        purple4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                purple4ActionPerformed(evt);
            }
        });
        jPanel1.add(purple4);
        purple4.setBounds(270, 10, 120, 110);

        yellow4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/yellow.png"))); // NOI18N
        yellow4.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        yellow4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                yellow4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                yellow4MouseExited(evt);
            }
        });
        yellow4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                yellow4ActionPerformed(evt);
            }
        });
        jPanel1.add(yellow4);
        yellow4.setBounds(240, 40, 130, 120);

        purple5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/purple.png"))); // NOI18N
        purple5.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        purple5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                purple5MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                purple5MouseExited(evt);
            }
        });
        purple5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                purple5ActionPerformed(evt);
            }
        });
        jPanel1.add(purple5);
        purple5.setBounds(40, 180, 110, 110);

        green5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/green.png"))); // NOI18N
        green5.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        green5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                green5MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                green5MouseExited(evt);
            }
        });
        green5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                green5ActionPerformed(evt);
            }
        });
        jPanel1.add(green5);
        green5.setBounds(60, 200, 130, 130);

        red5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/red.png"))); // NOI18N
        red5.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        red5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                red5MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                red5MouseExited(evt);
            }
        });
        red5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                red5ActionPerformed(evt);
            }
        });
        jPanel1.add(red5);
        red5.setBounds(20, 210, 130, 119);

        yellow5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/yellow.png"))); // NOI18N
        yellow5.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        yellow5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                yellow5MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                yellow5MouseExited(evt);
            }
        });
        yellow5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                yellow5ActionPerformed(evt);
            }
        });
        jPanel1.add(yellow5);
        yellow5.setBounds(30, 170, 130, 120);

        yellow6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/yellow.png"))); // NOI18N
        yellow6.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        yellow6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                yellow6MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                yellow6MouseExited(evt);
            }
        });
        yellow6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                yellow6ActionPerformed(evt);
            }
        });
        jPanel1.add(yellow6);
        yellow6.setBounds(310, 220, 130, 120);

        purple6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/purple.png"))); // NOI18N
        purple6.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        purple6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                purple6MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                purple6MouseExited(evt);
            }
        });
        purple6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                purple6ActionPerformed(evt);
            }
        });
        jPanel1.add(purple6);
        purple6.setBounds(250, 210, 110, 110);

        green6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/green.png"))); // NOI18N
        green6.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        green6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                green6MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                green6MouseExited(evt);
            }
        });
        green6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                green6ActionPerformed(evt);
            }
        });
        jPanel1.add(green6);
        green6.setBounds(290, 200, 120, 130);

        red6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/red.png"))); // NOI18N
        red6.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        red6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                red6MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                red6MouseExited(evt);
            }
        });
        red6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                red6ActionPerformed(evt);
            }
        });
        jPanel1.add(red6);
        red6.setBounds(240, 180, 130, 120);

        blue1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/blue.png"))); // NOI18N
        blue1.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        blue1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                blue1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                blue1MouseExited(evt);
            }
        });
        blue1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blue1ActionPerformed(evt);
            }
        });
        jPanel1.add(blue1);
        blue1.setBounds(470, 160, 90, 90);

        blue2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/blue.png"))); // NOI18N
        blue2.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        blue2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                blue2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                blue2MouseExited(evt);
            }
        });
        blue2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blue2ActionPerformed(evt);
            }
        });
        jPanel1.add(blue2);
        blue2.setBounds(450, 20, 90, 90);

        blue3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/blue.png"))); // NOI18N
        blue3.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        blue3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                blue3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                blue3MouseExited(evt);
            }
        });
        blue3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blue3ActionPerformed(evt);
            }
        });
        jPanel1.add(blue3);
        blue3.setBounds(460, 70, 90, 90);

        blue4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/blue.png"))); // NOI18N
        blue4.setToolTipText("Choose the color that matches the word being displayed\nex if green apears choose the green box");
        blue4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                blue4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                blue4MouseExited(evt);
            }
        });
        blue4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                blue4ActionPerformed(evt);
            }
        });
        jPanel1.add(blue4);
        blue4.setBounds(430, 130, 90, 90);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(64, 64, 64)
                .addComponent(ColorChoice)
                .addGap(115, 115, 115)
                .addComponent(TimeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(66, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 587, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addComponent(ColorChoice))
                    .addComponent(TimeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(16, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
///////////////////////////////////////////////////////////////////////////////////////////
    // method: different colorButtons
    // purpose: first assigns it a color then it will
    //verify if was the right color that was chosen
    private void yellowActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_yellowActionPerformed
        String color = "yellow";
        VerifyColor(color);
    }//GEN-LAST:event_yellowActionPerformed

    // method: different colorButtons
    // purpose: first assigns it a color then it will
    //verify if was the right color that was chosen
    private void redActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_redActionPerformed
        String color = "red";
        VerifyColor(color);
    }//GEN-LAST:event_redActionPerformed

    // method: different colorButtons
    // purpose: first assigns it a color then it will
    //verify if was the right color that was chosen
    private void purpleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_purpleActionPerformed
        String color = "purple";
        VerifyColor(color);
    }//GEN-LAST:event_purpleActionPerformed

///////////////////////////////////////////////////////////__________________________
    // method: different colorButtons
    // purpose: first assigns it a color then it will
    //verify if was the right color that was chosen
    private void green4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_green4ActionPerformed
        String color = "green";
        VerifyColor(color);
    }//GEN-LAST:event_green4ActionPerformed

    // method: different colorButtons
    // purpose: first assigns it a color then it will
    //verify if was the right color that was chosen
    private void purple4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_purple4ActionPerformed
        String color = "purple";
        VerifyColor(color);
    }//GEN-LAST:event_purple4ActionPerformed

    // method: different colorButtons
    // purpose: first assigns it a color then it will
    //verify if was the right color that was chosen
    private void red4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_red4ActionPerformed
        String color = "red";
        VerifyColor(color);
    }//GEN-LAST:event_red4ActionPerformed

    // method: different colorButtons
    // purpose: first assigns it a color then it will
    //verify if was the right color that was chosen
    private void yellow4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_yellow4ActionPerformed
        String color = "yellow";
        VerifyColor(color);
    }//GEN-LAST:event_yellow4ActionPerformed
////////////////////////////////////////////////////////////__________________________
// method: different colorButtons
    // purpose: first assigns it a color then it will
    //verify if was the right color that was chosen
    private void green5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_green5ActionPerformed
        String color = "green";
        VerifyColor(color);
    }//GEN-LAST:event_green5ActionPerformed
    // method: different colorButtons
    // purpose: first assigns it a color then it will
    //verify if was the right color that was chosen
    private void purple5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_purple5ActionPerformed
        String color = "purple";
        VerifyColor(color);
    }//GEN-LAST:event_purple5ActionPerformed
    // method: different colorButtons
    // purpose: first assigns it a color then it will
    //verify if was the right color that was chosen
    private void red5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_red5ActionPerformed
        String color = "red";
        VerifyColor(color);
    }//GEN-LAST:event_red5ActionPerformed
    // method: different colorButtons
    // purpose: first assigns it a color then it will
    //verify if was the right color that was chosen
    private void yellow5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_yellow5ActionPerformed
        String color = "yellow";
        VerifyColor(color);
    }//GEN-LAST:event_yellow5ActionPerformed
////////////////////////////////////////////////////////////__________________________
    // method: different colorButtons
    // purpose: first assigns it a color then it will
    //verify if was the right color that was chosen
    private void purple6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_purple6ActionPerformed
        String color = "purple";
        VerifyColor(color);
    }//GEN-LAST:event_purple6ActionPerformed
    // method: different colorButtons
    // purpose: first assigns it a color then it will
    //verify if was the right color that was chosen
    private void yellow6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_yellow6ActionPerformed
        String color = "yellow";
        VerifyColor(color);
    }//GEN-LAST:event_yellow6ActionPerformed
    // method: different colorButtons
    // purpose: first assigns it a color then it will
    //verify if was the right color that was chosen
    private void green6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_green6ActionPerformed
        String color = "green";
        VerifyColor(color);
    }//GEN-LAST:event_green6ActionPerformed
    // method: different colorButtons
    // purpose: first assigns it a color then it will
    //verify if was the right color that was chosen

  
    private void red6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_red6ActionPerformed
        String color = "red";
        VerifyColor(color);
    }//GEN-LAST:event_red6ActionPerformed

    
    
      ////////////////////---_________________________________________
    //the following code is rathe similar
    
    // method: mouseenetered
    // purpose: will change the background to a highlighted color
    
    //method: mouseexit
    //purpose: return the background of the button to normal 
    
    private void greenMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_greenMouseEntered
        green.setBackground(set);
    }//GEN-LAST:event_greenMouseEntered

    private void greenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_greenActionPerformed
        String color = "green";
        VerifyColor(color);
    }//GEN-LAST:event_greenActionPerformed

    private void greenMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_greenMouseExited
        green.setBackground(UIManager.getColor("control"));
    }//GEN-LAST:event_greenMouseExited

    private void purpleMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_purpleMouseEntered
         purple.setBackground(set);
    }//GEN-LAST:event_purpleMouseEntered

    private void redMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_redMouseEntered
       red.setBackground(set);
    }//GEN-LAST:event_redMouseEntered

    private void yellowMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_yellowMouseEntered
        yellow.setBackground(set);
    }//GEN-LAST:event_yellowMouseEntered

    private void yellowMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_yellowMouseExited
        yellow.setBackground(UIManager.getColor("control"));
    }//GEN-LAST:event_yellowMouseExited

    private void redMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_redMouseExited
       red.setBackground(UIManager.getColor("control"));
    }//GEN-LAST:event_redMouseExited

    private void purpleMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_purpleMouseExited
        purple.setBackground(UIManager.getColor("control"));
    }//GEN-LAST:event_purpleMouseExited

    private void jPanel1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanel1MouseExited

    private void red4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_red4MouseEntered
        red4.setBackground(set);
    }//GEN-LAST:event_red4MouseEntered

    private void red4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_red4MouseExited
        red4.setBackground(UIManager.getColor("control"));
    }//GEN-LAST:event_red4MouseExited

    private void green4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_green4MouseEntered
         green4.setBackground(set);
    }//GEN-LAST:event_green4MouseEntered

    private void green4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_green4MouseExited
        green4.setBackground(UIManager.getColor("control"));
    }//GEN-LAST:event_green4MouseExited

    private void purple4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_purple4MouseEntered
        purple4.setBackground(set);
    }//GEN-LAST:event_purple4MouseEntered

    private void purple4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_purple4MouseExited
       purple4.setBackground(UIManager.getColor("control"));
    }//GEN-LAST:event_purple4MouseExited

    private void yellow4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_yellow4MouseEntered
       yellow4.setBackground(set);
    }//GEN-LAST:event_yellow4MouseEntered

    private void yellow4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_yellow4MouseExited
             yellow4.setBackground(UIManager.getColor("control"));
    }//GEN-LAST:event_yellow4MouseExited

    private void yellow5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_yellow5MouseEntered
         yellow5.setBackground(set);
    }//GEN-LAST:event_yellow5MouseEntered

    private void yellow5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_yellow5MouseExited
                     yellow5.setBackground(UIManager.getColor("control"));

    }//GEN-LAST:event_yellow5MouseExited

    private void purple5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_purple5MouseEntered
         purple5.setBackground(set);
    }//GEN-LAST:event_purple5MouseEntered

    private void purple5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_purple5MouseExited
                     purple5.setBackground(UIManager.getColor("control"));

    }//GEN-LAST:event_purple5MouseExited

    private void green5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_green5MouseEntered
         green5.setBackground(set);
    }//GEN-LAST:event_green5MouseEntered

    private void green5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_green5MouseExited
                     green5.setBackground(UIManager.getColor("control"));

    }//GEN-LAST:event_green5MouseExited

    private void red5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_red5MouseEntered
         red5.setBackground(set);
    }//GEN-LAST:event_red5MouseEntered

    private void red5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_red5MouseExited
                     red5.setBackground(UIManager.getColor("control"));

    }//GEN-LAST:event_red5MouseExited

    private void red6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_red6MouseEntered
        red6.setBackground(set);
    }//GEN-LAST:event_red6MouseEntered

    private void red6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_red6MouseExited
                     red6.setBackground(UIManager.getColor("control"));
    }//GEN-LAST:event_red6MouseExited

    private void green6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_green6MouseEntered
        green6.setBackground(set);
    }//GEN-LAST:event_green6MouseEntered

    private void green6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_green6MouseExited
                    green6.setBackground(UIManager.getColor("control"));
    }//GEN-LAST:event_green6MouseExited

    private void purple6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_purple6MouseEntered
        purple6.setBackground(set);
    }//GEN-LAST:event_purple6MouseEntered

    private void purple6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_purple6MouseExited
                    purple6.setBackground(UIManager.getColor("control"));
    }//GEN-LAST:event_purple6MouseExited

    private void yellow6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_yellow6MouseEntered
         yellow6.setBackground(set);
    }//GEN-LAST:event_yellow6MouseEntered

    private void yellow6MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_yellow6MouseExited
        yellow6.setBackground(UIManager.getColor("control"));

    }//GEN-LAST:event_yellow6MouseExited

    private void blue2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blue2ActionPerformed
        String color = "blue";
        VerifyColor(color);
    }//GEN-LAST:event_blue2ActionPerformed

    private void blue4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blue4ActionPerformed
         String color = "blue";
        VerifyColor(color);
    }//GEN-LAST:event_blue4ActionPerformed

    private void blue3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blue3ActionPerformed
        String color = "blue";
        VerifyColor(color);
    }//GEN-LAST:event_blue3ActionPerformed

    private void blue1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_blue1ActionPerformed
         String color = "blue";
        VerifyColor(color);
    }//GEN-LAST:event_blue1ActionPerformed

    private void blue2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_blue2MouseEntered
          blue2.setBackground(set);
    }//GEN-LAST:event_blue2MouseEntered

    private void blue2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_blue2MouseExited
        blue2.setBackground(UIManager.getColor("control"));
    }//GEN-LAST:event_blue2MouseExited

    private void blue3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_blue3MouseEntered
        blue3.setBackground(set);
    }//GEN-LAST:event_blue3MouseEntered

    private void blue3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_blue3MouseExited
        blue3.setBackground(UIManager.getColor("control"));

    }//GEN-LAST:event_blue3MouseExited

    private void blue4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_blue4MouseEntered
        blue4.setBackground(set);
    }//GEN-LAST:event_blue4MouseEntered

    private void blue4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_blue4MouseExited
       blue4.setBackground(UIManager.getColor("control"));

    }//GEN-LAST:event_blue4MouseExited

    private void blue1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_blue1MouseEntered
        blue1.setBackground(set);
    }//GEN-LAST:event_blue1MouseEntered

    private void blue1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_blue1MouseExited
         blue1.setBackground(UIManager.getColor("control"));

    }//GEN-LAST:event_blue1MouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Hangman7Color.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Hangman7Color.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Hangman7Color.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Hangman7Color.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Hangman7Color().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel ColorChoice;
    private javax.swing.JLabel TimeLabel;
    private javax.swing.JButton blue1;
    private javax.swing.JButton blue2;
    private javax.swing.JButton blue3;
    private javax.swing.JButton blue4;
    private javax.swing.JButton green;
    private javax.swing.JButton green4;
    private javax.swing.JButton green5;
    private javax.swing.JButton green6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JButton purple;
    private javax.swing.JButton purple4;
    private javax.swing.JButton purple5;
    private javax.swing.JButton purple6;
    private javax.swing.JButton red;
    private javax.swing.JButton red4;
    private javax.swing.JButton red5;
    private javax.swing.JButton red6;
    private javax.swing.JButton yellow;
    private javax.swing.JButton yellow4;
    private javax.swing.JButton yellow5;
    private javax.swing.JButton yellow6;
    // End of variables declaration//GEN-END:variables
}
